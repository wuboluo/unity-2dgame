using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    public class AnimationController : Unit
    {
        public UnityEvent onStartEvents;
        public UnityEvent onCompleteEvents;

        [SerializeField] private CompleteType completeType;
        private Animator _animator;

        protected override void _Awake()
        {
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void OnEnable()
        {
            _animator = GetComponent<Animator>();
            DelayClipComplete().Continue();
        }

        private async Task DelayClipComplete()
        {
            onStartEvents?.Invoke();

            var time = (long)(_animator.GetCurrentClipLength() * 1000);
            await TimeHelper.WaitAsync(time);

            switch (completeType)
            {
                case CompleteType.KeepAppear:
                    break;

                case CompleteType.Disappear:
                    gameObject.SetActive(false);
                    break;

                case CompleteType.Destroy:
                    Destroy(gameObject);
                    break;
            }

            onCompleteEvents?.Invoke();
        }
    }

    public enum CompleteType
    {
        KeepAppear,
        Disappear,
        Destroy
    }
}