using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    public class AnimationEventInserter : MonoBehaviour
    {
        public UnityEvent momentEvent;

        public void Raise()
        {
            momentEvent?.Invoke();
        }
    }
}