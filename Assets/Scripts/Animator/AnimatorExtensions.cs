using UnityEngine;

namespace BUG
{
    public static class AnimatorExtensions
    {
        public static float GetClipLength(this Animator animator, string clipName)
        {
            if (null == animator || string.IsNullOrEmpty(clipName) || null == animator.runtimeAnimatorController)
            {
                return 0;
            }

            var runtimeController = animator.runtimeAnimatorController;
            var clips = runtimeController.animationClips;
            if (clips is not { Length: > 0 })
            {
                return 0;
            }

            for (var i = 0; i < clips.Length; i++)
            {
                var clip = runtimeController.animationClips[i];
                if (null != clip && clip.name == clipName)
                {
                    return clip.length;
                }
            }

            return 0f;
        }

        public static float GetCurrentClipLength(this Animator animator)
        {
            var clipInfo = animator.GetCurrentAnimatorClipInfo(0);
            var currentClipLength = clipInfo[0].clip.length;
            return currentClipLength;
        }
    }
}