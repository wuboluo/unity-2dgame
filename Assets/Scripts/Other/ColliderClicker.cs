using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    public class ColliderClicker : MonoBehaviour
    {
        public UnityEvent onClick;
        private bool _clicked;

        private void OnMouseDown()
        {
            if (_clicked)
            {
                return;
            }

            onClick?.Invoke();
            _clicked = true;
        }
    }
}