using System;

namespace BUG
{
    public class ValueWatcher<T> where T : IEquatable<T>
    {
        private T _currentValue;

        public ValueWatcher(T originalValue)
        {
            _currentValue = originalValue;
        }

        public void UpdateValue(T newValue, string changerName = "")
        {
            if (_currentValue.Equals(newValue))
            {
                return;
            }

            var oldValue = _currentValue;
            _currentValue = newValue;

            OnValueChanged(oldValue, newValue, changerName);
        }

        private static void OnValueChanged(T oldValue, T newValue, string changerName = "")
        {
            EventSystem.Instance.Publish(new ValueChanged<T>
            {
                oldValue = oldValue, newValue = newValue,
                changerName = changerName
            });
        }
    }
}