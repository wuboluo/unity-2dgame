using System;
using System.Threading.Tasks;

namespace BUG
{
    public static class TimeHelper
    {
        public static async Task WaitAsync(long time)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(time));
        }
    }
}