﻿using System.Collections.Generic;
using UnityEngine;

namespace BUG
{
    [CreateAssetMenu(fileName = "New ObjectPool", menuName = "BUG/Object Pool/New Object Pool SO")]
    public class ObjectPoolSO : ScriptableObject
    {
        public GameObject prefab;
        public int size;

        private Transform _unitRoot;
        private Stack<RecyclableObject> _pool;

        public void Initialize(Transform root)
        {
            _unitRoot = root;
            _pool = new Stack<RecyclableObject>();

            for (var i = 0; i < size; i++)
            {
                _pool.Push(New());
            }
        }

        public RecyclableObject Fetch()
        {
            var obj = _pool.Count > 0 ? _pool.Pop() : New();
            obj.Initialize(this);

            return obj;
        }

        public void Recycle(long unitId)
        {
            var obj = UnitSystem.Get(unitId) as RecyclableObject;
            if (obj != null)
            {
                _pool.Push(obj);
            }
        }

        private RecyclableObject New()
        {
            var obj = Instantiate(prefab, _unitRoot).GetComponent<RecyclableObject>();
            obj.gameObject.SetActive(false);

            return obj;
        }
    }
}