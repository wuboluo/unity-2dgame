﻿using System.Threading.Tasks;

namespace BUG
{
    public abstract class RecyclableObject : Unit
    {
        private ObjectPoolSO Pool { get; set; }

        public void Initialize(ObjectPoolSO pool)
        {
            Pool = pool;
            Appear();
        }

        private void Appear()
        {
            gameObject.SetActive(true);
        }

        public void _Disappear()
        {
            gameObject.SetActive(false);
            Pool.Recycle(Id);
        }

        public async Task _DisappearAsync(long time)
        {
            await TimeHelper.WaitAsync(time);
            _Disappear();
        }
    }
}