namespace BUG
{
    public enum BLogLevel
    {
        Info,
        Warning,
        Error,
        
        // 不显示任何日志，日志也很消耗性能
        None
    }
}