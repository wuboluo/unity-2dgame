using UnityEngine;

namespace BUG
{
    public static class BLog
    {
        public static BLogLevel CurrentLogLevel { get; set; } = BLogLevel.Info;

        public static void Info(string message)
        {
            if (CurrentLogLevel <= BLogLevel.Info)
            {
                Debug.Log($"INFO: {message}");
            }
        }

        public static void Warning(string message)
        {
            if (CurrentLogLevel <= BLogLevel.Warning)
            {
                Debug.LogWarning($"WARNING: {message}");
            }
        }

        public static void Error(string message)
        {
            if (CurrentLogLevel <= BLogLevel.Error)
            {
                Debug.LogError($"ERROR: {message}");
            }
        }
    }
}