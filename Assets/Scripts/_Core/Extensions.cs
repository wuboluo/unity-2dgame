using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BUG
{
    public static class Extensions
    {
        #region Task

        public static async void Continue(this Task task)
        {
            await task.ConfigureAwait(false);
        }

        #endregion

        #region UIBehaviour

        public static T GetOrAddComponent<T>(this UIBehaviour ui) where T : Component
        {
            var component = ui.GetComponent<T>();
            if (component == null)
            {
                component = ui.gameObject.AddComponent<T>();
            }

            return component;
        }

        public static void AddEventTrigger(this UIBehaviour ui, EventTriggerType triggerType, UnityAction<BaseEventData> callback)
        {
            var trigger = ui.GetOrAddComponent<EventTrigger>();
            var entry = new EventTrigger.Entry
            {
                eventID = triggerType
            };
            entry.callback.AddListener(callback);
            trigger.triggers.Add(entry);
        }

        #endregion
    }
}