using System.Collections.Generic;

namespace BUG
{
    public static class UnitSystem
    {
        public static readonly Dictionary<long, Unit> units;

        static UnitSystem()
        {
            units = new Dictionary<long, Unit>();
        }

        public static void Add(Unit unit)
        {
            units.TryAdd(unit.Id, unit);
        }

        public static void Remove(long id)
        {
            if (units.ContainsKey(id))
            {
                units.Remove(id);
            }
        }

        public static Unit Get(long id)
        {
            return units.TryGetValue(id, out var unit) ? unit : null;
        }
    }
}