using System;
using UnityEngine;

namespace BUG
{
    public abstract class Unit : MonoBehaviour
    {
        [SerializeField] private long previewId;
        public long Id { get; private set; }

        private void Awake()
        {
            Id = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            previewId = Id;

            UnitSystem.Add(this);
            _Awake();
        }

        private void OnDisable()
        {
            if (this is IEvent e)
            {
                e.Unregister();
            }

            _Disable();
        }

        private void OnDestroy()
        {
            _Destroy();
            UnitSystem.Remove(Id);
        }

        protected abstract void _Awake();
        protected abstract void _Disable();
        protected abstract void _Destroy();
    }
}