using UnityEngine;

namespace BUG
{
    public abstract class SingletonMono<T> : Unit where T : Unit
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject { name = typeof(T).ToString() };
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<T>();

                    return _instance;
                }

                return _instance;
            }
        }

        private void Awake()
        {
            _instance = this as T;
            _Awake();
        }
    }
}