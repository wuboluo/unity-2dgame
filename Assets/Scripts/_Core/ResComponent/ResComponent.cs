using UnityEngine;

namespace BUG
{
    public static class ResComponent
    {
        public static GameObject Load(string path, Vector3 position)
        {
            return Load(path, position, Quaternion.identity);
        }

        public static GameObject Load(string path, Vector3 position, Quaternion rotation)
        {
            var prefab = Resources.Load<GameObject>(path);
            if (prefab == null)
            {
                BLog.Error("Prefab not found!");
                return null;
            }

            return Object.Instantiate(prefab, position, rotation);
        }
    }
}