using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BUG
{
    public class Event
    {
        public Delegate Handler;
        public Type EventType;
    }

    public class EventSystem : Singleton<EventSystem>
    {
        public delegate void EventHandler<in T>(T t);

        private readonly Dictionary<long, List<Event>> _unitEvents = new();
        private readonly Dictionary<string, List<long>> _eventUnits = new();

        public void Publish<T>(T t) where T : struct
        {
            var eventType = t.GetType().FullName;
            if (string.IsNullOrEmpty(eventType) || !_eventUnits.TryGetValue(eventType, out var unitIds))
            {
                return;
            }

            foreach (var unitId in unitIds)
            {
                if (!_unitEvents.TryGetValue(unitId, out var events))
                {
                    continue;
                }

                foreach (var e in events.Where(tag => tag.EventType == typeof(T)))
                {
                    ((EventHandler<T>)e.Handler)(t);
                }
            }
        }

        public async Task PublishAsync<T>(T t, long time) where T : struct
        {
            await TimeHelper.WaitAsync(time);
            Publish(t);
        }

        public void Register<T>(EventHandler<T> handler) where T : struct
        {
            if (handler.Target is not IEvent e)
            {
                BLog.Error("Target is not a IEvent");
                return;
            }

            OnRegister(((Unit)e).Id, typeof(T).FullName, new Event
            {
                Handler = handler,
                EventType = typeof(T)
            });
        }

        public void Unregister(IEvent e)
        {
            var unitId = ((Unit)e).Id;

            if (_unitEvents.TryGetValue(unitId, out var events))
            {
                events.Clear();
                _unitEvents.Remove(unitId);

                var nullEventTypes = new List<string>();
                foreach (var (eventType, unitIds) in _eventUnits)
                {
                    if (unitIds.Contains(unitId))
                    {
                        unitIds.Remove(unitId);
                    }

                    if (unitIds.Count <= 0)
                    {
                        nullEventTypes.Add(eventType);
                    }
                }

                foreach (var _e in nullEventTypes)
                {
                    _eventUnits.Remove(_e);
                }
            }
        }

        private void OnRegister(long unitId, string eventType, Event e)
        {
            // unit - eventTypes
            if (!_unitEvents.TryGetValue(unitId, out var events))
            {
                events = new List<Event>();
                _unitEvents.Add(unitId, events);
            }
            else
            {
                if (events.Any(_e => _e.EventType == e.EventType && _e.Handler == e.Handler))
                {
                    return;
                }
            }

            events.Add(e);

            // eventType - units
            if (!_eventUnits.TryGetValue(eventType, out var unitIds))
            {
                unitIds = new List<long>();
                _eventUnits.Add(eventType, unitIds);
            }

            if (!unitIds.Contains(unitId))
            {
                unitIds.Add(unitId);
            }
        }
    }
}