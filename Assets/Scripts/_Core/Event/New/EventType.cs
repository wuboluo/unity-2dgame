namespace BUG
{
    public struct ValueChanged<T>
    {
        public T oldValue;
        public T newValue;

        public string changerName;
    }
}