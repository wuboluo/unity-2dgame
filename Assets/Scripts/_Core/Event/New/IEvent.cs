namespace BUG
{
    public interface IEvent
    {
        void Unregister()
        {
            EventSystem.Instance.Unregister(this);
        }
    }
}