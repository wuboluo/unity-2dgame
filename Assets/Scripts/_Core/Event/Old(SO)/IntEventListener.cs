﻿using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    [System.Serializable]
    public class IntEvent : UnityEvent<int>
    {
    }

    public class IntEventListener : MonoBehaviour
    {
        [SerializeField] 
        private IntEventChannelSO channel;
        public IntEvent onEventRaised;

        private void OnEnable()
        {
            if (channel != null) channel.OnEventRaised += Respond;
        }

        private void OnDisable()
        {
            if (channel != null) channel.OnEventRaised -= Respond;
        }

        private void Respond(int value)
        {
            onEventRaised?.Invoke(value);
        }
    }
}