﻿namespace BUG
{
    public class EventListener<T>
    {
        public delegate void OnValueChanged(T args);

        private T _value;

        public T Value
        {
            set
            {
                if (_value.Equals(value))
                {
                    return;
                }

                OnValueChangedEvent?.Invoke(value);
                _value = value;
            }
        }

        public event OnValueChanged OnValueChangedEvent;
    }
}