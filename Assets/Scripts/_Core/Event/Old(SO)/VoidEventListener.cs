﻿using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    public class VoidEventListener : MonoBehaviour
    {
        [SerializeField]
        private VoidEventChannelSO channel;
        public UnityEvent onEventRaised;

        private void OnEnable()
        {
            if (channel != null) channel.OnEventRaised += Respond;
        }

        private void OnDisable()
        {
            if (channel != null) channel.OnEventRaised -= Respond;
        }

        private void Respond()
        {
            onEventRaised?.Invoke();
        }
    }
}