﻿using UnityEngine.Events;
using UnityEngine;

namespace BUG
{
    [CreateAssetMenu(menuName = "BUG/Event SO/Bool Event Channel")]
    public class BoolEventChannelSO : ScriptableObject
    {
        public event UnityAction<bool> OnEventRaised;

        public void Raise(bool value)
        {
            OnEventRaised?.Invoke(value);
        }
    }
}