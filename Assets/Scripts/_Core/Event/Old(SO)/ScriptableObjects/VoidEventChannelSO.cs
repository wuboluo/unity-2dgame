﻿using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    [CreateAssetMenu(menuName = "BUG/Event SO/Void Event Channel")]
    public class VoidEventChannelSO : ScriptableObject
    {
        public UnityAction OnEventRaised;

        public void Raise()
        {
            OnEventRaised?.Invoke();
        }
    }
}