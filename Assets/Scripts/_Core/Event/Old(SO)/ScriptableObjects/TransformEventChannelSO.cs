﻿using UnityEngine.Events;
using UnityEngine;

namespace BUG
{
    [CreateAssetMenu(menuName = "BUG/Event SO/Transform Event Channel")]
    public class TransformEventChannelSO : ScriptableObject
    {
        public UnityAction<Transform> OnEventRaised;

        public void Raise(Transform value)
        {
            OnEventRaised?.Invoke(value);
        }
    }
}