﻿using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    [CreateAssetMenu(menuName = "BUG/Event SO/Float Event Channel")]
    public class FloatEventChannelSO : ScriptableObject
    {
        public UnityAction<float> OnEventRaised;

        public void Raise(float value)
        {
            OnEventRaised?.Invoke(value);
        }
    }
}