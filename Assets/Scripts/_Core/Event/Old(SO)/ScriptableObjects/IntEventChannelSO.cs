﻿using UnityEngine;
using UnityEngine.Events;

namespace BUG
{
    [CreateAssetMenu(menuName = "BUG/Event SO/Int Event Channel")]
    public class IntEventChannelSO : ScriptableObject
    {
        public UnityAction<int> OnEventRaised;

        public void Raise(int value)
        {
            OnEventRaised?.Invoke(value);
        }
    }
}