﻿using UnityEngine;

namespace BUG
{
    public class CameraAdapter : Unit, IEvent
    {
        private const float BgSize = 20.48f;
        private const float Proportion = 2f;

        [SerializeField] 
        private CameraProjection cameraProjection;

        private ValueWatcher<float> _ratioWatcher;

        public Camera MainCam { get; private set; }
        public float Ratio { get; private set; } = -1;
        private static float Scale => -BgSize / 2 / Mathf.Tan(30 * Mathf.Deg2Rad);

        protected override void _Awake()
        {
            MainCam = Camera.main;
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Start()
        {
            _ratioWatcher = new ValueWatcher<float>(Ratio);
            EventSystem.Instance.Register<ValueChanged<float>>(OnRatioChangedHandler);
        }

        private void Update()
        {
            Ratio = (float)Screen.width / Screen.height;
            _ratioWatcher.UpdateValue(Ratio, "MainCamera Ratio");
        }

        private void OnRatioChangedHandler(ValueChanged<float> arg)
        {
            BLog.Info($"{arg.changerName} : {arg.oldValue} → {arg.newValue}");
            AdapterViewSize();
        }

        private void AdapterViewSize()
        {
            switch (cameraProjection)
            {
                case CameraProjection.Perspective:
                    var z = Ratio < 1 ? Scale : Scale * Screen.height / Screen.width;
                    transform.position = new Vector3(0, 0, z);
                    break;

                case CameraProjection.Orthographic:
                    MainCam.orthographicSize = Ratio < 1 ? BgSize / Proportion : BgSize / Proportion / Ratio;
                    break;
            }
        }
    }

    public enum CameraProjection
    {
        Perspective,
        Orthographic
    }
}