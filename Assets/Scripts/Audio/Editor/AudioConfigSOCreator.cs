using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace BUG
{
    // 定义音频配置的编辑器窗口
    public class AudioConfigSOCreator : EditorWindow
    {
        // 菜单路径
        private const string MenuPath_Clip_Single = "Assets/Create/BUG/Audio/Single";
        private const string MenuPath_Clip_List = "Assets/Create/BUG/Audio/List";

        // 单个音频配置
        [MenuItem(MenuPath_Clip_Single)]
        public static void CreateSingleSourceConfig()
        {
            var selectObjs = Selection.objects;
            var folders = GetSelectAssetFolderPath();

            foreach (var obj in selectObjs)
            {
                var scAsset = CreateInstance<Single_SourceConfigSO>();
                scAsset.clip = obj as AudioClip;

                var scAssetName = $"ClipSingle {obj.name}.asset";
                var savePath = Path.Combine(folders.First(), scAssetName);

                SaveSourceConfigAsset(scAsset, savePath);
            }
        }

        // 一组音频配置
        [MenuItem(MenuPath_Clip_List)]
        public static void CreateRandomSourceConfig()
        {
            var selectObjs = Selection.objects;
            var folders = GetSelectAssetFolderPath();

            var scAsset = CreateInstance<List_SourceConfigSO>();
            scAsset.clips = selectObjs.OfType<AudioClip>().ToArray();

            var scAssetName = $"ClipList {selectObjs.First().name}~{selectObjs.Last().name}.asset";
            var savePath = Path.Combine(folders.First(), scAssetName);

            SaveSourceConfigAsset(scAsset, savePath);
        }

        // 验证选择的资源是否为音频文件
        [MenuItem(MenuPath_Clip_Single, true)]
        [MenuItem(MenuPath_Clip_List, true)]
        private static bool SelectedAsset()
        {
            return GetAssetExtension().All(e => e is "mp3" or "wav" or "ogg");
        }

        // 保存SO
        private static void SaveSourceConfigAsset(Object scAsset, string savePath)
        {
            AssetDatabase.CreateAsset(scAsset, savePath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        // 选中的资源路径
        private static string[] GetSelectAssetPath()
        {
            return Selection.objects.Select(value => AssetDatabase.GetAssetPath(value).Replace('/', Path.DirectorySeparatorChar)).ToArray();
        }

        // 资源扩展名
        private static string[] GetAssetExtension()
        {
            return GetSelectAssetPath().Select(value => Path.GetExtension(value).TrimStart('.')).ToArray();
        }

        // 选中资源所在的文件夹路径
        private static string[] GetSelectAssetFolderPath()
        {
            return GetSelectAssetPath().Select(value => value[..(value.LastIndexOf(Path.DirectorySeparatorChar) + 1)]).ToArray();
        }
    }
}