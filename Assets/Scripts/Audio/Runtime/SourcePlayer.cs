using UnityEngine;

namespace BUG
{
    [RequireComponent(typeof(AudioSource))]
    public class SourcePlayer : RecyclableObject
    {
        private AudioSource _source;

        public string Tag { get; private set; }

        protected override void _Awake()
        {
            _source = GetComponent<AudioSource>();
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        public bool Initialize(AudioClip clip, bool isLoop = false, string _tag = "", float volume = 1)
        {
            if (clip != null)
            {
                Tag = _tag;

                _source.clip = clip;
                _source.loop = isLoop;
                _source.volume = volume;
                _source.Play();

                if (!_source.loop)
                {
                    var time = (long)(clip.length * 1000);
                    _DisappearAsync(time).Continue();
                }

                return true;
            }

            return false;
        }

        public bool Play(Single_SourceConfigSO config)
        {
            return Initialize(config.clip, config.isLoop, config.tag, config.volume);
        }

        public bool PlayList(List_SourceConfigSO config)
        {
            if (config.clips.Length == 0)
            {
                return false;
            }

            var clip = GetClipFromList(config);
            return Initialize(clip, false, config.tag, config.volume);
        }

        private static AudioClip GetClipFromList(List_SourceConfigSO config)
        {
            var clipsLength = config.clips.Length;
            ref var lastIndex = ref config.lastPlayedIndex;

            switch (config.playMode)
            {
                case AudioPlayMode.Sequential:
                {
                    lastIndex = (lastIndex + 1) % clipsLength;
                    return config.clips[lastIndex];
                }
                case AudioPlayMode.Random:
                {
                    lastIndex = Random.Range(0, clipsLength);
                    return config.clips[lastIndex];
                }
                case AudioPlayMode.RandomNoImmediateRepeat:
                {
                    int newIndex;
                    do
                    {
                        newIndex = Random.Range(0, clipsLength);
                    } while (clipsLength > 1 && newIndex == lastIndex);

                    lastIndex = newIndex;
                    return config.clips[lastIndex];
                }

                default:
                    return null;
            }
        }
    }
}