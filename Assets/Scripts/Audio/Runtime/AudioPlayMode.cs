namespace BUG
{
    public enum AudioPlayMode
    {
        // 顺序播放
        Sequential,
        
        // 随机播放
        Random,
        
        // 随机但不立刻重复
        RandomNoImmediateRepeat
    }
}