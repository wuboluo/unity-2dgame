﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BUG
{
    // todo 每一种音频设置同时播放上限

    public class AudioSystem : SingletonMono<AudioSystem>
    {
        [SerializeField]
        private ObjectPoolSO sourcePool;
        private readonly List<SourcePlayer> _playingSources = new();

        protected override void _Awake()
        {
            sourcePool.Initialize(transform);
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        public void Play(ISource config)
        {
            var source = sourcePool.Fetch() as SourcePlayer;
            if (source != null)
            {
                var playSuccess = config switch
                {
                    Single_SourceConfigSO singleSO => source.Play(singleSO),
                    List_SourceConfigSO listSO => source.PlayList(listSO),
                    _ => false
                };

                if (playSuccess) _playingSources.Add(source);
            }
        }

        public void Stop(params string[] _tags)
        {
            if (_tags is not { Length: > 0 })
            {
                return;
            }

            for (var i = _playingSources.Count - 1; i >= 0; i--)
            {
                var source = _playingSources[i];
                if (_tags.Any(_tag => source.Tag == _tag))
                {
                    OnComplete(source);
                }
            }
        }

        public void StopAll()
        {
            for (var i = _playingSources.Count - 1; i >= 0; i--)
            {
                var source = _playingSources[i];
                OnComplete(source);
            }
        }

        private void OnComplete(RecyclableObject source)
        {
            if (source is SourcePlayer player)
            {
                if (_playingSources.Contains(player))
                {
                    _playingSources.Remove(player);
                    source._Disappear();
                }
            }
        }
    }
}