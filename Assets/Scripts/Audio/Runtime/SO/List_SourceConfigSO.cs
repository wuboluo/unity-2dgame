using UnityEngine;

namespace BUG
{
    public class List_SourceConfigSO : ScriptableObject, ISource
    {
        public AudioClip[] clips;
        [Range(0, 1)] public float volume = 1;
        public string tag;
      
        public AudioPlayMode playMode;
        public int lastPlayedIndex;
    }
}