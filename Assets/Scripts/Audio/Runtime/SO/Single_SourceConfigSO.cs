﻿using UnityEngine;

namespace BUG
{
    public class Single_SourceConfigSO : ScriptableObject, ISource
    {
        public AudioClip clip;
        [Range(0, 1)] public float volume = 1;
        public string tag;

        public bool isLoop;
    }
}