using UnityEngine;
using UnityEngine.UI;

namespace BUG
{
    public class AudioPlayer_Sample : MonoBehaviour
    {
        public Single_SourceConfigSO singleConfig;
        public List_SourceConfigSO listConfig;

        public Button btn_singlePlay;
        public Button btn_listPlay;

        private void Start()
        {
            listConfig.lastPlayedIndex = listConfig.clips.Length - 1;

            btn_singlePlay.onClick.AddListener(() => AudioSystem.Instance.Play(singleConfig));
            btn_listPlay.onClick.AddListener(() => AudioSystem.Instance.Play(listConfig));
        }
    }
}