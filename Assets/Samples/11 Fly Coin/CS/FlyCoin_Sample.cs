using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace BUG
{
    public class FlyCoin_Sample : MonoBehaviour
    {
        public GameObject coinPrefab;
        public Transform start;
        public Transform target;
        public int coinsAmount = 10;
        public int offset = 100;

        private RectTransform[] _coins;

        private void Start()
        {
            start.GetComponent<Button>().onClick.AddListener(Fly);
        }

        private void Init()
        {
            var random = new Random();
            _coins = new RectTransform[coinsAmount];

            for (var i = 0; i < coinsAmount; i++)
            {
                var randomPos = start.position + new Vector3(random.Next(-offset, offset), random.Next(-offset, offset), 0);
                var randomRot = Quaternion.Euler(new Vector3(0, 0, random.Next(0, 360)));
                var coin = Instantiate(coinPrefab, randomPos, randomRot, start.parent).GetComponent<RectTransform>();

                coin.gameObject.SetActive(false);
                coin.localScale = Vector3.zero;

                _coins[i] = coin;
            }
        }

        private void Fly()
        {
            Init();

            var delay = 0.0f;
            for (var i = 0; i < coinsAmount; i++)
            {
                var coin = _coins[i];
                coin.gameObject.SetActive(true);

                coin.DOScale(Vector3.one, 0.3f).SetDelay(delay).SetEase(Ease.OutBack);
                coin.DOMove(target.position, 0.8f).SetDelay(delay + 0.5f).SetEase(Ease.InBack);
                coin.DORotateQuaternion(Quaternion.identity, 0.5f).SetDelay(delay + 0.5f).SetEase(Ease.Flash);
                coin.DOScale(Vector3.zero, 0.3f).SetDelay(delay + 1.5f).SetEase(Ease.InBack).OnComplete(() => Recycle(coin));
                
                delay += 0.1f;
            }
        }

        private void Recycle(Component coin)
        {
            Destroy(coin.gameObject);
        }
    }
}