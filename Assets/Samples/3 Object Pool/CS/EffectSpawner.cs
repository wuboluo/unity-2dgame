﻿using UnityEngine;

namespace BUG
{
    public class EffectSpawner : MonoBehaviour
    {
        [SerializeField] 
        private ObjectPoolSO effectPool;
        private Camera _mainCam;

        private void Start()
        {
            _mainCam = Camera.main;
            effectPool.Initialize(transform);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Spawn();
            }
        }

        private void Spawn()
        {
            if (!Physics.Raycast(_mainCam.ScreenPointToRay(Input.mousePosition), out var hit))
            {
                return;
            }

            if (!hit.collider || !hit.collider.CompareTag("EffectCollider"))
            {
                return;
            }

            if (effectPool.Fetch() is ClickEffect effect)
            {
                effect.Initialize(_mainCam);
            }
        }
    }
}