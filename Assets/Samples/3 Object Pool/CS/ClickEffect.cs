﻿using UnityEngine;

namespace BUG
{
    [RequireComponent(typeof(Animator))]
    public class ClickEffect : RecyclableObject
    {
        private Animator _animator;

        protected override void _Awake()
        {
            _animator = GetComponent<Animator>();
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        public void Initialize(Camera mainCam)
        {
            transform.position = mainCam.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);

            var time = (long)(_animator.GetCurrentClipLength() * 1000);
            _DisappearAsync(time).Continue();
        }
    }
}