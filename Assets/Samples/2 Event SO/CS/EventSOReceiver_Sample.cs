using UnityEngine;

namespace BUG
{
    public class EventSOReceiver_Sample : MonoBehaviour
    {
        public IntEventChannelSO channel;

        private void Start()
        {
            channel.OnEventRaised += Test;
        }

        private void OnDestroy()
        {
            channel.OnEventRaised -= Test;
        }

        private void Test(int value)
        {
            BLog.Info(name + " " + value);
        }
    }
}