using UnityEngine;
using UnityEngine.UI;

namespace BUG
{
    public class EventSOSender_Sample : MonoBehaviour
    {
        public Button btn_Call;
        public IntEventChannelSO channel;

        private const int value = 9999;

        private void Start()
        {
            btn_Call.onClick.AddListener(Call);
        }

        private void Call()
        {
            channel.Raise(value);
        }
    }
}