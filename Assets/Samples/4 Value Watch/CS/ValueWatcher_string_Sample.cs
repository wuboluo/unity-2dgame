using TMPro;
using UnityEngine;
using Random = System.Random;

namespace BUG
{
    public class ValueWatcher_string_Sample : Unit, IEvent
    {
        public TMP_InputField input;
        public Random random ;

        private ValueWatcher<string> _watcher;
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        protected override void _Awake()
        {
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Start()
        {
            _watcher = new ValueWatcher<string>(input.text);
            random = new Random();

            EventSystem.Instance.Register<ValueChanged<string>>(TestEventHandler);
        }

        private void Update()
        {
            _watcher.UpdateValue(input.text, input.name);

            if (Input.GetKeyDown(KeyCode.Q))
            {
                input.text = chars[random.Next(chars.Length)].ToString();
            }
        }

        private void TestEventHandler(ValueChanged<string> arg)
        {
            BLog.Info($"{arg.changerName} : {arg.oldValue} → {arg.newValue}");
        }
    }
}