using UnityEngine;
using UnityEngine.UI;

namespace BUG
{
    public class ValueWatcher_float_Sample : Unit, IEvent
    {
        public Slider slider_1, slider_2;
        private ValueWatcher<float> _watcher_1, _watcher_2;

        protected override void _Awake()
        {
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Start()
        {
            _watcher_1 = new ValueWatcher<float>(slider_1.value);
            _watcher_2 = new ValueWatcher<float>(slider_2.value);

            EventSystem.Instance.Register<ValueChanged<float>>(TestEventHandler);
        }

        private void Update()
        {
            _watcher_1.UpdateValue(slider_1.value, slider_1.name);
            _watcher_2.UpdateValue(slider_2.value, slider_2.name);

            if (Input.GetKeyDown(KeyCode.Q))
            {
                slider_1.value = Random.Range(0f, 1);
                slider_2.value = Random.Range(0f, 1);
            }
        }

        private void TestEventHandler(ValueChanged<float> arg)
        {
            BLog.Info($"{arg.changerName} : {arg.oldValue} → {arg.newValue}");
        }
    }
}