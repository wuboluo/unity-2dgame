using System.Threading.Tasks;
using UnityEngine.UI;

namespace BUG
{
    public class EventSender_Sample : Unit
    {
        private const int value = 9999;

        public Button btn_sync;
        public Button btn_async_1;
        public Button btn_async_2;

        protected override void _Awake()
        {
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Start()
        {
            btn_sync.onClick.AddListener(P0);
            btn_async_1.onClick.AddListener(() => P1().Continue());
            btn_async_2.onClick.AddListener(P2);
        }
        
        void P0()
        {
            EventSystem.Instance.Publish(new TestEvent { Number = value });
            BLog.Info("P");
        }

        async Task P1()
        {
            await PublishAsync();
            BLog.Info("use await");
        }

        void P2()
        {
            PublishAsync().Continue();
            BLog.Info("use continue");
        }

        async Task PublishAsync()
        {
            await EventSystem.Instance.PublishAsync(new TestEvent { Number = value }, 1000);
        }
    }
}