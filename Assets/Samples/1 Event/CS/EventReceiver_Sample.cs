namespace BUG
{
    public class EventReceiver_Sample : Unit, IEvent
    {
        protected override void _Awake()
        {
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Start()
        {
            EventSystem.Instance.Register<TestEvent>(TestEventHandler);
        }

        private void TestEventHandler(TestEvent arg)
        {
            BLog.Info(name + " " + arg.Number);
        }
    }
}