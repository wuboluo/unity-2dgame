using UnityEngine;

namespace BUG
{
    public class Mover_Sample : Unit, IEvent
    {
        public float speed = 0.5f;

        private const float _speedRatio = 10;
        private float _camWidth, _camHeight;
        private Vector3 _direction;

        protected override void _Awake()
        {
            EventSystem.Instance.Register<ValueChanged<Vector2>>(ChangeDirection);

            var camSize = Camera.main!.orthographicSize;
            _camWidth = camSize * ((float)Screen.width / Screen.height);
            _camHeight = camSize;
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void Update()
        {
            var pos = transform.position + _direction * (speed / _speedRatio * Time.deltaTime);
            pos.x = Mathf.Clamp(pos.x, -_camWidth, _camWidth);
            pos.y = Mathf.Clamp(pos.y, -_camHeight, _camHeight);

            transform.position = pos;
        }

        private void ChangeDirection(ValueChanged<Vector2> arg)
        {
            _direction = arg.newValue;
        }
    }
}