using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BUG
{
    public class Joystick_Sample : Unit
    {
        public JoystickType joystickType;

        public Image touchArea;
        public Image joystickRange;
        public Image joystick;

        private float _radius;
        private ValueWatcher<Vector2> _watcher;

        protected override void _Awake()
        {
            touchArea.AddEventTrigger(EventTriggerType.PointerDown, PointerDown);
            touchArea.AddEventTrigger(EventTriggerType.PointerUp, PointerUp);
            touchArea.AddEventTrigger(EventTriggerType.Drag, Drag);

            _radius = joystickRange.rectTransform.sizeDelta.x / 2f;
            _watcher = new ValueWatcher<Vector2>(Vector2.zero);

            joystickRange.gameObject.SetActive(joystickType == JoystickType.Fixed);
        }

        protected override void _Disable()
        {
        }

        protected override void _Destroy()
        {
        }

        private void PointerDown(BaseEventData data)
        {
            if (joystickType == JoystickType.Nimble)
            {
                joystickRange.gameObject.SetActive(true);

                if (ToLocalPoint(touchArea.rectTransform, data, out var localPoint))
                {
                    joystickRange.transform.localPosition = localPoint;
                }
            }
        }

        private void PointerUp(BaseEventData data)
        {
            joystick.transform.localPosition = Vector2.zero;
            _watcher.UpdateValue(Vector2.zero);

            if (joystickType == JoystickType.Nimble)
            {
                joystickRange.gameObject.SetActive(false);
            }
        }

        private void Drag(BaseEventData data)
        {
            if (ToLocalPoint(joystickRange.rectTransform, data, out var localPoint))
            {
                joystick.transform.localPosition = localPoint.magnitude > _radius
                    ? localPoint.normalized * _radius
                    : localPoint;

                _watcher.UpdateValue(localPoint);
            }
        }

        private static bool ToLocalPoint(RectTransform rect, BaseEventData data, out Vector2 localPoint)
        {
            if (data is PointerEventData eventData
                && RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, eventData.position, eventData.pressEventCamera, out var _localPoint))
            {
                localPoint = _localPoint;
                return true;
            }

            localPoint = default;
            return false;
        }
    }
}